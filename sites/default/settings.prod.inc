<?php

// read the credentials file
$string = file_get_contents($_ENV['CRED_FILE'], false);
if ($string == false) {
    die('FATAL: Could not read credentials file');
}

// the file contains a JSON string, decode it and return an associative array
$creds = json_decode($string, true);

$databases = array (
  'default' => array (
    'default' => array (
      'database' => getenv('CLEARDB_DATABASE_NAME'),
      'username' => getenv('CLEARDB_USERNAME'),
      'password' => getenv('CLEARDB_PASSWORD'),
      'host' => getenv('CLEARDB_DATABASE_URL'),
      'port' => '3306',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

?>