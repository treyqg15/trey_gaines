
<?php

// read the credentials file
$string = file_get_contents($_ENV['CRED_FILE'], false);
if ($string == false) {
    die('FATAL: Could not read credentials file');
}

// the file contains a JSON string, decode it and return an associative array
$creds = json_decode($string, true);

$databases = array (
  'default' => array (
    'default' => array (
      'database' => $creds["MYSQLS"]["MYSQLS_DATABASE"],
      'username' => $creds["MYSQLS"]["MYSQLS_USERNAME"],
      'password' => $creds["MYSQLS"]["MYSQLS_PASSWORD"],
      'host' => $creds["MYSQLS"]["MYSQLS_HOSTNAME"],
      'port' => '3306',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

?>