/* global $, document */
$(document).ready(function() {
    
    var uiManager = (function() {
        
        var SINGLE_ELIMINATION = "SINGLE_ELIMINATION";
        var DOUBLE_ELIMINATION = "DOUBLE_ELIMINATION";
        
        var tabs = $( "#tabs" ).tabs(),
        tabTitle = $( "#tab_title" ),
        tabContent = $( "#tab_content" ),
        tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>Remove Tab</span></li>",
        tabCounter = 2;
        
        var bracket = function() {
        
            var singleElimination = {
                "teams": [              // Matchups
                    ["Team 1", "Team 2"], // First match
                    ["Team 3", "Team 4"]  // Second match
                ]/*,
                "results": [            // List of brackets (single elimination, so only one bracket)
                [                     // List of rounds in bracket
                  [                   // First round in this bracket
                    [1, 2],           // Team 1 vs Team 2
                    [3, 4]            // Team 3 vs Team 4
                  ],
                  [                   // Second (final) round in single elimination bracket
                    [5, 6],           // Match for first place
                    [7, 8]            // Match for 3rd place
                  ]
                ]
                ]
                */
            };

            var doubleElimination = {
                "teams": [
                    ["Team 1", "Team 2"],
                    ["Team 3", "Team 4"]
                  ],
                  "results": [            // List of brackets (three since this is double elimination)
                    [                     // Winner bracket
                      [[null, null], [null, null]]   // First round and results
                    ],
                    [                     // Loser bracket
                      [[null, null]],           // First round
                      [[null, null]]           // Second round
                    ]
                  ]

            };

            return {

                initSingleEliminationBracket : function (bracket) {
                    $(bracket).bracket({
                        init: singleElimination, /* data to initialize the bracket with */ 
                        save: saveFn
                    });                    
                },

                initDoubleEliminationBracket : function (bracket) {
                    $(bracket).bracket({
                        init: doubleElimination, /* data to initialize the bracket with */ 
                        save: saveFn
                    });
                }
            };
        };
        
        function addTab() {
            var label = tabTitle.val() || "Tab " + tabCounter,
            newBracket = bracket(),
            id = "tabs-" + tabCounter,
            bracketID = "bracket-" + tabCounter,
            li = $( tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) ),
            tabContentHtml = "<div id=" + bracketID + ">";//tabContent.val() || "Tab " + tabCounter + " content.";
            tabs.find( ".ui-tabs-nav" ).append( li );
            tabs.append( "<div id='" + id + "'><p>" + tabContentHtml + "</p></div>" );
            switch($("#bracket_type").val())
            {
                case SINGLE_ELIMINATION:
                    newBracket.initSingleEliminationBracket("#" + bracketID);
                    break;
                case DOUBLE_ELIMINATION:
                    newBracket.initDoubleEliminationBracket("#" + bracketID);
                    break;
            }
            
            tabs.tabs( "refresh" );
            tabCounter++;
        }
        
        return {
            init: function() {
                
                $("#tabs").tabs().css({
                   'min-height': '350px',
                   'overflow': 'auto'
                });
                
                // modal dialog init: custom buttons and a "close" callback resetting the form inside
                var dialog = $( "#dialog" ).dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                        Add: function() {
                            addTab();
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function() {
                        form[ 0 ].reset();
                    }
                });

                // addTab form: calls addTab function on submit and closes the dialog
                var form = dialog.find( "form" ).submit(function( event ) {
                    addTab();
                    dialog.dialog( "close" );
                    event.preventDefault();
                });

                // addTab button: just opens the dialog
                $( "#add_single_elimination_tab" )
                    .button()
                    .click(function() {
                        $("#bracket_type").val(SINGLE_ELIMINATION);
                        dialog.dialog( "open" );
                });
                
                $( "#add_double_elimination_tab" )
                    .button()
                    .click(function() {
                        $("#bracket_type").val(DOUBLE_ELIMINATION);
                        dialog.dialog( "open" );
                });
                
                // close icon: removing the tab on click
                tabs.delegate( "span.ui-icon-close", "click", function() {
                    var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
                    $( "#" + panelId ).remove();
                    tabs.tabs( "refresh" );
                });
                
                tabs.bind( "keyup", function( event ) {
                    if ( event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE ) {
                        var panelId = tabs.find( ".ui-tabs-active" ).remove().attr( "aria-controls" );
                        $( "#" + panelId ).remove();
                        tabs.tabs( "refresh" );
                    }            
                });
                
                var firstBracket = bracket();
                firstBracket.initSingleEliminationBracket('#bracket-1'); 
            },   
            
            getBracket: function() {
                return bracket();
            }
        };
    })();
    
    uiManager.init();    
    
    
});

function saveFn(data, userData) {
  var json = $.toJSON(data);
  $('#saveOutput').text('POST '+userData+' '+json);
  /* You probably want to do something like this
  jQuery.ajax("rest/"+userData, {contentType: 'application/json',
                                dataType: 'json',
                                type: 'post',
                                data: json})
  */
}
    