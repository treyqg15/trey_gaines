<?php
    
?>

<script src="//code.jquery.com/jquery-latest.min.js"></script>
<script src="js/jq/bracket_master/dist/jquery.bracket.min.js"></script>
<script src="js/index.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="js/jq/bracket_master/dist/jquery.bracket.min.css" />
<link rel="stylesheet" type="text/css" href="css/index.css" />

<div id="bracket_content">
    <h1>Bracket Program</h1>
    <input id="bracket_type" type="hidden" />
    <div id="dialog" title="Tab data">
        <form>
            <fieldset class="ui-helper-reset">
            <label for="tab_title">Title</label>
            <input type="text" name="tab_title" id="tab_title" value="" class="ui-widget-content ui-corner-all">
            <label for="tab_content">Content</label>
            <textarea name="tab_content" id="tab_content" class="ui-widget-content ui-corner-all"></textarea>
            </fieldset>
        </form>
    </div>
    <button id="add_single_elimination_tab">Create Single Elimination Bracket</button>
    <button id="add_double_elimination_tab">Create Double Elimination Bracket</button>
    <div id="tabs">
<ul>
<li><a href="#tabs-1">Bracket #1</a></li>
</ul>
<div id="tabs-1">
<div class="my_bracket" id="bracket-1"></div>
</div>

</div>
    <div class="my_bracket" id="single_elimination_bracket"></div>
    <div class="my_bracket" id="double_elimination_bracket"></div>
    
</div>